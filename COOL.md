# Browsing COOL data (AtlCoolConsole)
This document provides instructions on how to setup and use the `AtlCoolConsole` tool in order to browse COOL data. The tool is a python package which is available in athena: [AtlCoolConsole.py](https://gitlab.cern.ch/atlas/athena/-/blob/main/Database/CoolConvUtilities/share/AtlCoolConsole.py?ref_type=heads). 
There is also a TWiki page for [AtlCoolConsole](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/AtlCoolConsole).

The ATLAS database group have also developed a couple of systems for dynamic browsing of ATLAS COOL folders and tags. These interfaces help to get an overview of ATLAS data in COOL as well as details of specific folders and relationship to COOL Global Tags.
- COMA ([ConditionsMetadata](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/ConditionsMetadata#COMA_Conditions_DB_Folder_Tag_Br)) collects COOL Folder and Tag metadata for the [COMA Conditions DB Browser](https://atlas-tagservices.cern.ch/RBR/rBR_CB_Report.php) (authentication: install grid certificate on your browser)
- [CoolR](https://atlas-muon-align-mon.web.cern.ch/alignui/cooltags) is a [COOLR REST API](https://gitlab.cern.ch/formica/coolR/-/blob/master/swagger_schemas/swagger/yaml/coolrapi_full.yaml?ref_type=heads)

# Table of contents
- [Setup AtlCoolConsole on lxplus](#setup-atlcoolconsole-on-lxplus)
- [How to use AtlCoolConsole to browse data](#how-to-use-atlcoolconsole-to-browse-data)

## Setup AtlCoolConsole on lxplus
This instructions are valid inside CERN GPN, on an lxplus machine. 
```shell
setupATLAS
asetup Athena,main,latest
```
The tool should now be available, and you can see the available commands:
```shell
AtlCoolConsole.py
>>> help
...
>>> quit
```
If this works you can now connect to a COOL database, and browse COOL data.

## How to use AtlCoolConsole to browse data
The first step is to connect to the database you want to browse. The connection implies that you know which schema and database you want to access. 
We show below some options for connection strings, that you can test to see if you can access a database. 
```shell
# Shortcut connection string (defaults to a Frontier connection):
AtlCoolConsole.py "COOLONL_GLOBAL/CONDBR2"
...
Connected to 'frontier://ATLF/();schema=ATLAS_COOLONL_GLOBAL;dbname=CONDBR2'
Welcome to AtlCoolConsole. Type 'help' for instructions.
>>> ls
  Name              Description     
  /GLOBAL                           
>>> quit
...
# To see Frontier server setup:
echo $FRONTIER_SERVER
(serverurl=http://atlasfrontier-local.cern.ch:8000/atlr)(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://ca-proxy-atlas.cern.ch:3128)(proxyurl=http://ca-proxy-meyrin.cern.ch:3128)(proxyurl=http://ca-proxy.cern.ch:3128)(proxyurl=http://atlasbpfrontier.cern.ch:3127)
...
# Full connection string (you need the have the reader password for the following)
AtlCoolConsole.py "oracle://ATONR_ADG;schema=ATLAS_COOLONL_LAR;dbname=CONDBR2;user=ATLAS_COOL_READER;password=${COOL_READER_PASSWORD}"
```
Let's spend one moment to explain the `long` version of the connection string. The first part is selecting the technology you are using (`oracle`, `sqlite`), which is written like a `protocol` in a *URL*. The rest of the *URL* represent:
1. *ATONR_ADG* : is the Oracle cluster used. This one is a read-only cluster.
2. *schema*=.... : here you write the schema name (e.g. `ATLAS_COOLOFL_LAR`).
3. *dbname*=.... : here you write the database name (e.g. `CONDBR2`, `OFLP200`, ...).
4. *user*=... : the user name (in this case a generic reader).
5. *password*=...: the password for that user.

We describe below the schema names and dbname fields in more details.

| generic schema | description | 
|-----------------|-----------------|
| ATLAS_COOLONL_MDT  | The MDT schema for HLT usage (ONL)  | 
| ATLAS_COOLOFL_MDT  | The MDT schema for Bulk processing  | 
| ATLAS_COOLOFL_DCS | The schema with all DCS data |

In general the schema name is thus composed by a combination of COOLONL (COOLOFL) and a specific system name that you can pick among the following (not complete) list of names : MDT, MUONALIGN, TILE, PIXEL, INDET, GLOBAL, TDAQ, TRIGGER, DCS, CALO, SCT, ...

About the dbname:

| dbname | description | 
|-----------------|-----------------|
| CONDBR2  | The COOL db for physics data (>=Run2) | 
| OFLP200  | The COOL db for Montecarlo data  | 
| COMP200 | The COOL db for Run1 data |

All recent conditions for data are then in CONDBR2, and in OFLP200 for MC campaigns. 

### Browse the nodes (or folders)
The first step you can do is to verify the folders inside the selected schema/db. When you are connected to (e.g.) `ATLAS_COOLONL_LAR`, you will get something like this:
```shell
AtlCoolConsole.py "COOLONL_LAR/CONDBR2"
Connected to 'frontier://ATLF/();schema=ATLAS_COOLONL_LAR;dbname=CONDBR2'
Welcome to AtlCoolConsole. Type 'help' for instructions.
>>> ls
  Name              Description
  /LAR
>>> ls /LAR
  Name              Description
  /LAR/BadChannels
  /LAR/Configuration
  /LAR/ElecCalibFlat
  /LAR/ElecCalibFlatSC
  /LAR/ElecCalibOnl
  /LAR/Identifier
  /LAR/LArPileup
  Name              Description       Count     Size
  /LAR/Align        <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="254546453" /></addrHeader><typeName>DetCondKeyTrans</typeName>  -         -
  /LAR/LArCellPositionShift  <timeStamp>run-lumi</timeStamp><key>LArCellPositionShift</key><addrHeader><address_header service_type="71" clid="96609121" /></addrHeader><typeName>CaloRec::CaloCellPositionShift</typeName>  -         -
``` 
The data for a given `payload` type are stored in separate folders. But you need to provide the full path to that folder in order to check its content. In the output above you see that some folders have a sort of description and others do not have it. This is because those folders path are not yet complete.
```shell
>>> ls /LAR/BadChannels
  Name              Description       Count     Size
  /LAR/BadChannels/BadChannels  <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>  -         -
  /LAR/BadChannels/BadChannelsOnl  <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName><updateMode>UPD1</updateMode>  -         -
  /LAR/BadChannels/BadChannelsSC  <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>  -         -
  /LAR/BadChannels/DisabledSuperCells  <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>  -         -
  /LAR/BadChannels/EventVeto  <timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="40774348" /></addrHeader><typeName>AthenaAttributeList</typeName>  -         -
```
This will then show you all folders *contained* in `/LAR/BadChannels`. We have to stress the fact that these are not folders like in your file system, it is just a DB representation of something similar. In reality that name (e.g. `/LAR/BadChannels/EventVeto`) is just a string in a column of a specific table.

Have a look now at a *description* string. It contains some essential informations for Athena. One of those is the meaning of the `interval` for that folder: if you see `<timeStamp>time</timeStamp>` it means that your intervals are using an integer which represents a time (in COOL in general you have nanonseconds since EPOCH). If you see `<timeStamp>run-lumi</timeStamp>` it means that the integer represents a combination of run and lumi block (`run << 32 | lumi_block`, this corresponds to a 64 bits integer which is what is stored in COOL tables for run-lumi folders). By shifting a run number 32 bits on the left you will get a 64 bits integer, the lower 32 bits you fill them with the lumi_block part (via the binary 'OR' operator). If you have the COOL 64 bits integer you can go back to run and lumi_block by doing: `run = cool_since >> 32 ; lumi_block = 0x00000000ffffffff & cool_since`. 

The analogy with the file system can be noticed also when using another command, `cd`, which emulates (like `ls`) the behavior of unix commands.

```shell
>>> cd /LAR/BadChannels/BadChannels
>>> listinfo .
Specification for multi-version folder /LAR/BadChannels/BadChannels
 ChannelSize (UInt32)
 StatusWordSize (UInt32)
 Endianness (UInt32)
 Version (UInt32)
 Blob (Blob64k)
Description: <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>
```
The `listinfo` command allows to retrieve all information stored about that folder, in particular the format of the payload. Remember that when doing `cd xxxx` you are not really changing directory, it is just faking this for similarity with unix. 
You can obviously get the same content by doing:
```shell
>>> listinfo /LAR/BadChannels/BadChannels
```

### Browse the tags in a node
When you have chosen the folder you want to look at, you can start exploring the content of the payload data, and the intervals of validity (`IOVs`) in which the data are stored. Remember that this is
necessary only if the folder is marked as `multi-version` (check the output of `listinfo` above...)
```shell
>>> cd /LAR/BadChannels/BadChannels
>>> listtags .
Listing tags for folder /LAR/BadChannels/BadChannels
LARBadChannelsBadChannels-RUN2-UPD1-00 (locked) []
```

### Browse the IOVs in a tag
You have the tag, and can now start browsing `IOVs` and the payloads. 
```shell
>>> usetag LARBadChannelsBadChannels-RUN2-UPD1-00
Changed current tag selection to LARBadChannelsBadChannels-RUN2-UPD1-00
>>> less .
Using tag selection: LARBadChannelsBadChannels-RUN2-UPD1-00
  [918092209192960,933317868257280[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2014-07-31_10:24:29.398573000 GMT
  [933317868257280,937892008427520[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2014-07-31_10:24:29.398573000 GMT
  [937892008427520,937896303394816[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2014-07-31_10:24:29.398573000 GMT
....
```
If you want to see the complete payload you have to use `more` instead of `less` as a command. Be careful because some payloads can be very big, and some list of `IOVs` very long.
If you know in advance your data you can restrict the `IOVs` selection by providing a range in `time` (either timestamps or run lumi):

```shell
>>> userunlumi 
Usage: userunlumi <run1> {<LB1> <run2> <LB2>}
>>> userunlumi 462523 0 482523 0
# Using 'more' you see the full content in the selected range, the since and until times stored in COOL are represented as [run,lumiblock]
>>> more .
Using rawIOV range [1986521158647808,2072420504567808]
Using tag selection: LARBadChannelsBadChannels-RUN2-UPD1-00
[462523,0] - [470584,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2936,chk=110046099]
[470584,0] - [479010,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2936,chk=110046099]
[479010,0] - [479097,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2944,chk=980763771]
[479097,0] - [479360,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2936,chk=110046099]
[479360,0] - [482430,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2944,chk=980763771]
[482430,0] - [482463,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2944,chk=980763771]
[482463,0] - [485090,0) (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [Endianness (UInt32) : 0], [Version (UInt32) : 1], [Blob (Blob64k) : size=2944,chk=980763771]

### The same data can be browsed via 'less' command, this time you see the stored values as since and until, corresponding to (run << 32 | lumiblock)

>>> less .
Using rawIOV range [1986521158647808,2072420504567808]
Using tag selection: LARBadChannelsBadChannels-RUN2-UPD1-00
  [1986521158647808,2021142890020864[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-03-19_12:17:13.598584000 GMT
  [2021142890020864,2057332284456960[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-06-26_09:18:15.534022000 GMT
  [2057332284456960,2057705946611712[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-06-27_08:06:55.016021000 GMT
  [2057705946611712,2058835523010560[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-07-01_15:22:39.326416000 GMT
  [2058835523010560,2072021072609280[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-08-14_08:10:18.655290000 GMT
  [2072021072609280,2072162806530048[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-08-14_14:44:26.225911000 GMT
  [2072162806530048,2083445685616640[ (0) [ChannelSize (UInt32) : 4], [StatusWordSize (UInt32) : 4], [... 2024-09-22_19:16:11.485603000 GMT

```
Similarly for a `time` based folder you can use `usetimes time1 time2` in order to limit the range in time. The format of your argument can be a string representing a date : `2021-10-31:00:00:00`.

