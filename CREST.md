# CREST client examples

Examples for CREST API usage. We provide below instructions on using `crest-cmd` project as a client for browsing data in CREST.
For most examples we include a simple `curl` command line which does the same. We do not provide instruction for `curl` installation.
Sometimes we provide as well examples using `curl` and `jq` combined to parse the CREST response (which is always in json). Both commands are available on lxplus, but can be available as well on most machines with a minor effort to install them. `jq` is a `C` command line utility providing huge flexibility in parsing JSON. 

If you are testing on your own laptop you will need to install:
1. git
2. jq
3. curl

# Table of contents
- [Install crestCmd on lxplus](#install-crestcmd-on-lxplus)
- [Install crestCmd on your own laptop](#install-crestcmd-on-your-own-laptop)
- [How to use crestCmd to retrieve data](#how-to-use-crestcmd-to-retrieve-data)
- [How to use crestCmd to store data](#how-to-use-crestcmd-to-store-data)
- [How to use crestCmd to clean data](#how-to-use-crestcmd-to-clean-data)

## Install crestCmd on lxplus
This instructions are valid inside CERN GPN, and for a machine where `cvmfs` is accessible.
1. Use CrestApi from TDAQ common, for example
```shell
ls /cvmfs/atlas-online-nightlies.cern.ch/tdaq/nightlies/tdaq-common/tdaq-common-99-01-87/installed/external/x86_64-el9-gcc13-opt/lib
```
Be careful to have a coherent set of env variable adapted to that version. Here is a list of things that you will need to set on a "fresh" lxplus environment.
```shell
export TDAQ_RELEASE=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-12-00-00
export LD_LIBRARY_PATH=${TDAQ_RELEASE}/installed/external/x86_64-el9-gcc13-opt/lib:${LD_LIBRARY_PATH}
export CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:${TDAQ_RELEASE}/installed/external/x86_64-el9-gcc13-opt
source /cvmfs/atlas-nightlies.cern.ch/repo/sw/main_Athena_x86_64-el9-gcc13-opt/sw/lcg/releases/gcc/13.1.0/x86_64-el9/setup.sh
export CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:/cvmfs/atlas-nightlies.cern.ch/repo/sw/main_Athena_x86_64-el9-gcc13-opt/sw/lcg/releases/LCG_106b_ATLAS_1/Boost/1.86.0/x86_64-el9-gcc13-opt/lib/cmake
```
2. Clone crest-cmd package:
```shell
git clone  -b release-5.1 ssh://git@gitlab.cern.ch:7999/crest-db/crest-cmd.git
cd crest-cmd
mkdir ./build
```
3. Compile the package (needs to find a correct version for CrestApi library)
You can now execute the step to compile the package by moving to the `build` directory you created before and executing `cmake` and `make`.
```shell
cd build
cmake ..
make
```
4. Set a CREST_API_URL variable (CREST Server URL) use the command:
```shell
export CREST_API_URL=http://crest-j23.cern.ch:8080
```
Be careful that this server is ment for testing, and it could well be that sometimes it is down. We maintain a certain number of servers, so if this URL does not work please ask ADAM team about which server to use.

You can now go to the [testing](#how-to-use-crestcmd-to-retrieve-data) section.

## Install crestCmd on your own laptop:
We provide these instructions in case you want to test from your laptop and eventually use it outside CERN GPN (via a SOCKS proxy). This time you need to install as well `CrestApi` in case `cvmfs` is not available. If you have completed the step above you do not need to go through this section.

1. Install CrestApi from git
```shell
git clone  -b release-5.1-CR1 ssh://git@gitlab.cern.ch:7999/crest-db/CrestApi.git
cd CrestApi
mkdir ./build
## create a local installation directory: here we assume it will be in $HOME/local_lib
mkdir $HOME/local_lib
cd ./build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/local_lib ..
make && make install
```
You can check the `README` file for further information if needed. 

2. Clone crest-cmd package:
```shell
git clone  -b release-5.1 ssh://git@gitlab.cern.ch:7999/crest-db/crest-cmd.git
cd crest-cmd
mkdir ./build
```
3. Compile the package (needs to find a correct version for CrestApi library)
You can now execute the step to compile the package by moving to the `build` directory you created before and executing `cmake` and `make`.
```shell
cd build
cmake ..
make
```
4. Set a CREST_API_URL variable (CREST Server URL) use the command:
```shell
export CREST_API_URL=http://crest-j23.cern.ch:8080
```
In case you are outside CERN GPN, in order to access the server you will need to activate a tunnel.
This can be done in several ways:
```shell
ssh <username>@lxplus.cern.ch -D3129
``` 
You will then need to set an appropriate env variable to use the SOCKS proxy:
```shell
export SOCKS_PROXY=localhost:3129
```
You can now go to the [testing](#how-to-use-crestCmd) section.

## How to use crestCmd to retrieve data
You may need to add `.` to your `PATH` environment variable to run the following (or prepend `./` to each command below):
```shell
crestCmd get commands 
# will give you something like...
Command List:

1) Get list methods:

  crestCmd get tagList [OPTIONS]
  crestCmd get globalTagList [OPTIONS]
  crestCmd get globalTagMap [OPTIONS]
  crestCmd get iovList [OPTIONS]

2) Get methods:

  crestCmd get tag [OPTIONS]
  crestCmd get tagMetaInfo [OPTIONS]
  crestCmd get globalTag [OPTIONS]
  crestCmd get payload [OPTIONS]
  crestCmd get payloadMetaInfo [OPTIONS]
  crestCmd get payloadTagInfo [OPTIONS]
  crestCmd get tagSize [OPTIONS]

3) Create methods:

  crestCmd create tag [OPTIONS]
  crestCmd create tagMetaInfo [OPTIONS]
  crestCmd create globalTag [OPTIONS]
  crestCmd create globalTagMap [OPTIONS]
  crestCmd create iovAndPayload [OPTIONS]

4) Remove methods:

  crestCmd remove tag [OPTIONS]
  crestCmd remove globalTag [OPTIONS]
```
### List the tags
Dump a list of existing tags using some simple regexp to find what you are looking for:
```shell
crestCmd get tagList -h
crestCmd get tagList -n TEST%25
```
In `curl` you can do:
```shell
curl "http://crest-j23.cern.ch:8080/api-v5.0/tags?name=TEST%25" | jq -r '.resources[].name'
```

Please notice the string `%25`: this is the percent-encoded representation of `%`, which is used in URL encodings. The `%` character is used in DB queries as an equivalent of the shell `*`.
Since CREST API is an HTTP interface, we use that character as a *wildcard*.

### Get a specific Tag
Retrieve a tag and its metadata information using its name:
```shell
crestCmd get tag -h
crestCmd get tag -n LARBadChannelsMissingFEBs-RUN2-UPD1-01
crestCmd get tagMetaInfo -n LARBadChannelsMissingFEBs-RUN2-UPD1-01 
```
In `curl` you can do:
```shell
curl "http://crest-j23.cern.ch:8080/api-v5.0/tags?name=LARBadChannelsMissingFEBs-RUN2-UPD1-01" | jq -r '.resources[].name'
## or ...
curl "http://crest-j23.cern.ch:8080/api-v5.0/tags/LARBadChannelsMissingFEBs-RUN2-UPD1-01" 
## now get meta information, and extract the payload_spec field from the content of tagInfo
curl "http://crest-j23.cern.ch:8080/api-v5.0/tags/LARBadChannelsMissingFEBs-RUN2-UPD1-01/meta" | jq '.resources[] | {tagName, payload_spec: (.tagInfo | fromjson | .payload_spec)}'
```

### Select IOVs from a Tag
When you want to know the content of a `Tag` you can start by selecting `IOVs`. 
The concept of `IOV` in CREST is that a given payload is valid from a `since` until the next `since` ... so by default the last `since` is in principle valid until infinity.
The data you will retrieve appear as a list of `since` (this is a number representing what you want to use as time unit, could be seconds from unixtime or a combination of run/lumi or simply a run number, the meaning of your number should be described by the `timeType` field of the `Tag`).
To retrieve `IOVs` you can use for example:
```shell
crestCmd get iovList --tagName LARBadChannelsMissingFEBs-RUN2-UPD1-01
```
The options available in this command allow you to perform a selection in IOV range (`--since xxx --until yyyy`), and also to limit the number of output elements (the default is 20).

In `curl` you can do:
```shell
curl "http://crest-j23.cern.ch:8080/api-v5.0/iovs?tagname=LARBadChannelsMissingFEBs-RUN2-UPD1-01" | jq -r '.resources[].since'
## or ...
curl "http://crest-j23.cern.ch:8080/api-v5.0/iovs?tagname=LARBadChannelsMissingFEBs-RUN2-UPD1-01&since=924912617259008&until=1551359367184384"
```
The CREST API implements a pagination strategy for IOVs because their number can be large. The pagination arguments are `page` (representing the page number) and `size` (representing the number of elements in a page). If you want to retrieve IOVs in batches of 100 you can then use: `page=0 size=100`, and for the next 100 `page=1 size=100`. 
Both arguments are available in `crestCmd` and in `curl` as well. Another important argument (not implemented in `crestCmd`) is the possibility to order, for example if you want the last entry in the tag above you can do:
```shell
curl "http://crest-j23.cern.ch:8080/api-v5.0/iovs?tagname=LARBadChannelsMissingFEBs-RUN2-UPD1-01&page=0&size=1&sort=id.since:DESC"
```

The main purpose of retrieving `IOVs` is to have a key to access the underlying payload data (the conditions that are associated to an interval). This can be done via the `payloadHash` associated to each `IOV`.

Sometimes is useful to know how many `IOVs` are present in a given `Tag` without retrieving them:
```shell
crestCmd get tagSize -n LARBadChannelsMissingFEBs-RUN2-UPD1-01
# or 
curl "http://crest-j23.cern.ch:8080/api-v5.0/iovs/size?tagname=LARBadChannelsMissingFEBs-RUN2-UPD1-01"
```

### Retrieve a Payload
In order to retrieve a payload you need a `hash` key as described in the step above. Each payload has a certain amount of metadata associated. 
You can retrieve the metadata and the payload in the following way:
```shell
crestCmd get payloadMetaInfo -w be706ca483373487286dfef4bf5d5481458b536d08077aab237dbed707374edb
crestCmd get payload -w be706ca483373487286dfef4bf5d5481458b536d08077aab237dbed707374edb
```
In general the payload is a JSON file containing the conditions data for a given `Tag`. This is in any case not a mandatory constraint for CREST server, you could imagine that your payload is for example an image or a ROOT file. However, for a better Athena integration we have been using a standard JSON format to migrate existing COOL data.

CREST API provides as well some endpoints to get a more general overview of a `Tag` content. For example you may want to know for a given `Tag` the number of IOVs and the average and total volume:
```shell
crestCmd get payloadTagInfo -n LARBadChannelsMissingFEBs-RUN2-UPD1-01
# or ...
curl "http://crest-j23.cern.ch:8080/api-v5.0/monitoring/payloads?tagname=LARBadChannelsMissingFEBs-RUN2-UPD1-01&size=10000&page=0&sort=tagname:ASC"
# or for all LAR tags:
curl "http://crest-j23.cern.ch:8080/api-v5.0/monitoring/payloads?tagname=LAR%25&size=10000&page=0&sort=tagname:ASC" | jq -r '.resources[]'
```
These methods are useful to get an idea of tags with large volumes (in payload size or number of iovs), and it is easy to get a quick view using `jq`:
```shell
# select avgvolume > 1 MB...
curl "http://crest-j23.cern.ch:8080/api-v5.0/monitoring/payloads?tagname=LAR%25&size=10000&page=0&sort=tagname:ASC" | jq -r '.resources[] | select(.avgvolume > 1000000) | {tagname, avgvolume}'
# select niovs > 1000 ...
curl "http://crest-j23.cern.ch:8080/api-v5.0/monitoring/payloads?tagname=LAR%25&size=10000&page=0&sort=tagname:ASC" | jq -r '.resources[] | select(.niovs > 1000) | {tagname, niovs}'
```
### Get a Global Tag (or a list of Global Tags)
The concept of `Global Tag` is the same as in COOL but it is implemented in a very different manner.
A `Global Tag` is a sort of `container` of `Tags`. 
You can retrieve the `Global Tags` in the following way:
```shell
# get a list of all global tags containing the string HLT
crestCmd get globalTagList -n "%25HLT%25"
# or with curl...
curl "http://crest-j23.cern.ch:8080/api-v5.0/globaltags?name=%25HLT%25&size=20&page=0&sort=name:ASC" | jq
```

### Get a list of Tags in a Global Tag
The core utility of a `Global Tag` is to provide a list of associated `Tags`. Here is how you can get this information:
```shell
crestCmd get globalTagMap -n "CREST-HLTP-2024-02"
# or with curl
curl -H "X-Crest-MapMode: Trace" "http://crest-j23.cern.ch:8080/api-v5.0/globaltagmaps/CREST-HLTP-2024-02" | jq
```
Another useful information is to determine to which `Global Tags` a given `Tag` is associated:
```shell
crestCmd get globalTagMap -n "LARIdentifierCalibIdMap-RUN2-000" -m BackTrace
# or with curl
curl -H "X-Crest-MapMode: BackTrace" "http://crest-j23.cern.ch:8080/api-v5.0/globaltagmaps/LARIdentifierCalibIdMap-RUN2-000" | jq
```

## How to use crestCmd to store data
This part of the examples covers simple methods to store data in CREST. Be aware that for the payload part the examples we provide here are sort of `low level` methods, and should only be used for the purpose of testing. Also you should always create data in CREST in a way that is easy to identify as a TEST when you run the commands, and you may probably add your username to find more quickly your data.

We provide the examples in a precise order to have a complete overview of the steps starting from a `Global Tag`. In general system experts will only deal with `Tags`, as the `Global Tag` are normally administered by `Conditions Coordinators`.

### Create a Global Tag
To create a `Global Tag` you can use the following command (replace USERX with your name):
```shell
crestCmd create globalTag --tagName TEST-USERX-GT-CREST-01 --description test --release 1 --scenario test --type t --workflow T --validity 0
```
When using `curl` you should create a json file with that content:
```shell
cat > gt.json << EOF
{
    "description": "test",
    "name": "TEST-USERX-GT-CREST-02",
    "release": "1",
    "scenario": "test",
    "type": "t",
    "validity": "0",
    "workflow": "T"
}
EOF
# now upload the file with curl
curl -X POST http://crest-j23.cern.ch:8080/api-v5.0/globaltags \
     -H "Content-Type: application/json" \
     --data-binary @gt.json
# Verify that the global tag is created
curl -X GET http://crest-j23.cern.ch:8080/api-v5.0/globaltags/TEST-USERX-GT-CREST-01
```
Later on you will see how you can remove your tests to clean up what you did.


### Create a tag
To create a `Tag` you can use the following command:
```shell
crestCmd create tag --tagName TEST-USERX-LARAlign-01 --description "a test tag from userx" --timeType run-lumi
```
We are not going to provide here the `curl` example, but you can obviously do something very similar respect to what was shown before, generating a json file and then uploading it.

### Create Global Tag mapping
Since we have both a `Tag` and a `Global Tag` we can as well emulate what the `Conditions Coordination` would do, and associate our tag to the global tag.
```shell
crestCmd create globalTagMap --globalTagName TEST-USERX-GT-CREST-01 --tagName TEST-USERX-LARAlign-01 --label /LAR/Align --record None
```
You can check what you did using the commands from the previous section.

### Create tag meta info
For the purpose of running in Athena and to preserve some compatibility with the metadata provided in COOL, we need now to add some information to our `Tag`, in order to describe its content and the channels that each payload will contain.
```shell
crestCmd create tagMetaInfo --tagName TEST-USERX-LARAlign-01 --description "{\"dbname\":\"CREST\",\"nodeFullpath\":\"/LAR/Align\",\"schemaName\":\"LAR\"}" --chanList "[{\"0\":\"channel_0\"}]" --payloadSpec "[{\"PoolRef\":\"String4k\"}]" --nodeDescription "<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\\\"71\\\" clid=\\\"254546453\\\" /></addrHeader><typeName>DetCondKeyTrans</typeName>" 
```
In the `description` we put some information which is evocative of COOL schemas. This part is not relevant for Athena. Then we provide the channel list (`chanList`) and how many fields that payload will have (`payloadSpec`); finally we provide some COOL information for Athena (in the `nodeDescription` key). 

### Create IOVs and payloads
Having a `Tag` we can now store data in it. Be careful here that the example is we provide would not work for Athena, where you need an accurate format of your payload (or you need to provide a payload deserializer for Athena). In order to store data we obviously need as well to provide a time of validity. For simplicity here we use a `fake` run number.
```shell
crestCmd create iovAndPayload --tagName TEST-USERX-LARAlign-01 --payload "[DB=26B208DE-79E8-E611-9796-02163E019096][CNT=CollectionTree(DetCondKeyTrans//LAR/Align)][CLID=A6DA54AC-D54E-4B6C-8489-C6D015277AD0][TECH=00000202][OID=00000003-00000000]" --since 123456 --endTime 0
```
Note: `--endTime` is optional. The hash code for the payload is generated automatically, and you can use the methods described in the previous section to retrieve `IOVs` and `Payload` data.
```shell
 crestCmd get iovList -n TEST-USERX-LARAlign-01
 crestCmd get payload -w ad19a1b4ebce65eebbe7b9fa1f454016e0eebb93a42b69e7f639737de0230382
```

## How to use crestCmd to clean data
Once your tests are over, it would be nice if you could clean your data (if they are not useful for something else you want to do). For the moment we provide the following examples without requiring any authentication, but please do not remove things that others may have created. Later on we will not give clients any access to these methods, and the creation methods above will require authentication and authorization. Since we are using a test database we do not bother too much for the moment.

### Remove the Tag from the Global Tag
To detach a `Tag` from its `Global Tag` you can use:
```shell
crestCmd remove tagFromGlobalTagMap -n TEST-USERX-LARAlign-01 -g TEST-USERX-GT-CREST-01 -l "/LAR/Align"
```
You need to specify all parameters.

### Remove a Tag
Removing a `Tag` implies that we want to remove as well *ALL* the `IOVs` and `Payloads` for that `Tag`.
You can remove a `Tag` using:
```shell
crestCmd remove tag -n TEST-USERX-LARAlign-01
```
You can verify that the tag is not anymore available.

### Remove a Global Tag
If a `Global Tag` is not present in any association with some `Tag`, you may also remove it:
```shell
crestCmd remove globalTag -n TEST-USERX-GT-CREST-01 
# If you used the curl command for global tag creation you may also remove TEST-USERX-GT-CREST-02
```


